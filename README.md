# Laravel Dev Tools

[![Latest Stable Version](http://poser.pugx.org/mohamedhk2/laravel-dev-tools/v)](https://packagist.org/packages/mohamedhk2/laravel-dev-tools)
[![Total Downloads](http://poser.pugx.org/mohamedhk2/laravel-dev-tools/downloads)](https://packagist.org/packages/mohamedhk2/laravel-dev-tools)
[![Latest Unstable Version](http://poser.pugx.org/mohamedhk2/laravel-dev-tools/v/unstable)](https://packagist.org/packages/mohamedhk2/laravel-dev-tools)
[![License](http://poser.pugx.org/mohamedhk2/laravel-dev-tools/license)](https://packagist.org/packages/mohamedhk2/laravel-dev-tools)

## Packages

|                                                                                                               |                                                                                                                                               |
|---------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------|
| [amirami/localizator](https://packagist.org/packages/amirami/localizator)                                     | ![](http://poser.pugx.org/amirami/localizator/v) ![](http://poser.pugx.org/amirami/localizator/downloads)                                     |
| [barryvdh/laravel-debugbar](https://packagist.org/packages/barryvdh/laravel-debugbar)                         | ![](http://poser.pugx.org/barryvdh/laravel-debugbar/v) ![](http://poser.pugx.org/barryvdh/laravel-debugbar/downloads)                         |
| [barryvdh/laravel-ide-helper](https://packagist.org/packages/barryvdh/laravel-ide-helper)                     | ![](http://poser.pugx.org/barryvdh/laravel-ide-helper/v) ![](http://poser.pugx.org/barryvdh/laravel-ide-helper/downloads)                     |
| [bestmomo/laravel5-artisan-language](https://packagist.org/packages/bestmomo/laravel5-artisan-language)       | ![](http://poser.pugx.org/bestmomo/laravel5-artisan-language/v) ![](http://poser.pugx.org/bestmomo/laravel5-artisan-language/downloads)       |
| [kitloong/laravel-migrations-generator](https://packagist.org/packages/kitloong/laravel-migrations-generator) | ![](http://poser.pugx.org/kitloong/laravel-migrations-generator/v) ![](http://poser.pugx.org/kitloong/laravel-migrations-generator/downloads) |
| [laravel-lang/lang](https://packagist.org/packages/laravel-lang/lang)                                         | ![](http://poser.pugx.org/laravel-lang/lang/v) ![](http://poser.pugx.org/laravel-lang/lang/downloads)                                         |
| [mtolhuys/laravel-schematics](https://packagist.org/packages/mtolhuys/laravel-schematics)                     | ![](http://poser.pugx.org/mtolhuys/laravel-schematics/v) ![](http://poser.pugx.org/mtolhuys/laravel-schematics/downloads)                     |
| [nunomaduro/collision](https://packagist.org/packages/nunomaduro/collision)                                   | ![](http://poser.pugx.org/nunomaduro/collision/v) ![](http://poser.pugx.org/nunomaduro/collision/downloads)                                   |
| [orangehill/iseed](https://packagist.org/packages/orangehill/iseed)                                           | ![](http://poser.pugx.org/orangehill/iseed/v) ![](http://poser.pugx.org/orangehill/iseed/downloads)                                           |
| [reliese/laravel](https://packagist.org/packages/reliese/laravel)                                             | ![](http://poser.pugx.org/reliese/laravel/v) ![](http://poser.pugx.org/reliese/laravel/downloads)                                             |

## Install

The recommended way to install this is through composer:

```bash
composer require --dev "mohamedhk2/laravel-dev-tools:1.0.2"
```

## License

The Laravel Dev Tools is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)
