# Laravel Dev Tools

[![Latest Stable Version](http://poser.pugx.org/mohamedhk2/laravel-dev-tools/v)](https://packagist.org/packages/mohamedhk2/laravel-dev-tools)
[![Total Downloads](http://poser.pugx.org/mohamedhk2/laravel-dev-tools/downloads)](https://packagist.org/packages/mohamedhk2/laravel-dev-tools)
[![Latest Unstable Version](http://poser.pugx.org/mohamedhk2/laravel-dev-tools/v/unstable)](https://packagist.org/packages/mohamedhk2/laravel-dev-tools)
[![License](http://poser.pugx.org/mohamedhk2/laravel-dev-tools/license)](https://packagist.org/packages/mohamedhk2/laravel-dev-tools)

## Packages

|                                                                                                               |                                                                                                                                               |
|---------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------|
| [barryvdh/laravel-debugbar](https://packagist.org/packages/barryvdh/laravel-debugbar)                         | ![](http://poser.pugx.org/barryvdh/laravel-debugbar/v) ![](http://poser.pugx.org/barryvdh/laravel-debugbar/downloads)                         |
| [barryvdh/laravel-ide-helper](https://packagist.org/packages/barryvdh/laravel-ide-helper)                     | ![](http://poser.pugx.org/barryvdh/laravel-ide-helper/v) ![](http://poser.pugx.org/barryvdh/laravel-ide-helper/downloads)                     |
| [bestmomo/laravel5-artisan-language](https://packagist.org/packages/bestmomo/laravel5-artisan-language)       | ![](http://poser.pugx.org/bestmomo/laravel5-artisan-language/v) ![](http://poser.pugx.org/bestmomo/laravel5-artisan-language/downloads)       |
| [kitloong/laravel-migrations-generator](https://packagist.org/packages/kitloong/laravel-migrations-generator) | ![](http://poser.pugx.org/kitloong/laravel-migrations-generator/v) ![](http://poser.pugx.org/kitloong/laravel-migrations-generator/downloads) |
| [laravel-lang/common](https://packagist.org/packages/laravel-lang/common)                                     | ![](http://poser.pugx.org/laravel-lang/common/v) ![](http://poser.pugx.org/laravel-lang/common/downloads)                                     |
| [nunomaduro/collision](https://packagist.org/packages/nunomaduro/collision)                                   | ![](http://poser.pugx.org/nunomaduro/collision/v) ![](http://poser.pugx.org/nunomaduro/collision/downloads)                                   |
| [orangehill/iseed](https://packagist.org/packages/orangehill/iseed)                                           | ![](http://poser.pugx.org/orangehill/iseed/v) ![](http://poser.pugx.org/orangehill/iseed/downloads)                                           |
| [reliese/laravel](https://packagist.org/packages/reliese/laravel)                                             | ![](http://poser.pugx.org/reliese/laravel/v) ![](http://poser.pugx.org/reliese/laravel/downloads)                                             |

#### Removed packages

| package                                                                                                            | replaced by                                                                                                  | version | reason                    |
|--------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------|---------|---------------------------|
| <del>[mtolhuys/laravel-schematics](https://packagist.org/packages/mtolhuys/laravel-schematics)</del>               | None                                                                                                         | ^1.0.3  | Not support PHP ^8.x      |
| <del>[amirami/localizator](https://packagist.org/packages/amirami/localizator)</del>                               | None                                                                                                         | ^1.0.4  | No stable version         |
| <del>[laravel-lang/lang](https://packagist.org/packages/laravel-lang/lang)</del>                                   | [laravel-lang/common](https://packagist.org/packages/laravel-lang/common)                                    | ^1.0.5  | Upgrade package           |
| <del>[bestmomo/laravel5-artisan-language](https://packagist.org/packages/bestmomo/laravel5-artisan-language)</del> | None                                                                                                         | ^1.0.6  | Not support Laravel ^11.x |
| <del>[bestmomo/laravel5-artisan-language](https://packagist.org/packages/bestmomo/laravel5-artisan-language)</del> | [MostafaNaguib/laravel-artisan-language](https://github.com/MostafaNaguib/laravel-artisan-language) `(fork)` | ^1.1.0  | Not support Laravel ^11.x |

## Install

The recommended way to install this is through composer:

```bash
composer require --dev "mohamedhk2/laravel-dev-tools:1.1.0"
```

## Laravel Artisan Language

- Config file `config/artisan-language.php` <sup>*(`^1.1.0`)*</sup> :
```php
use \Mohamedhk2\LaravelDevTools\Classes\RegexConfig;

return [
    'scan_paths' => [
        app_path(),
        resource_path('views'),
        ...
    ],
    'lang_path' => base_path('lang'),
    #
    'patterns' => [
        new RegexConfig('/(@lang|__|\$t|\$tc)\s*(\(\s*[\'"])([^$]*)([\'"]+\s*(,[^\)]*)*\))/U', 3),
        new RegexConfig('/(?:trans|__)\s*\(\s*(?:"((?:[^"]|\\")+)"|\'((?:[^\']|\\\')+)\')\s*(?:,\s*[^)]*\s*)?\)/U', 1, function ($regConf, $out) {
            /**
             * @var $regConf RegexConfig
             */
            return array_values(array_filter(array_merge($out[1], $out[2])));
        }),
    ]
];
```

- usage : [README](https://github.com/bestmomo/laravel-artisan-language?tab=readme-ov-file#readme)

## License

The Laravel Dev Tools is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)
